## 1. Instalación/Ejecución
### Prerrequisitos:
Tener instalado npm y NodeJS
### Instalar:
```
npm install (to install express and other dependencies)
```
### Correr:
```
npm run dev
ó
nodemon index.js
```
## 3. Detalles
Correr en el puerto 3000