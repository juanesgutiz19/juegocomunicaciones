const path = require('path');
const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const formatMessage = require('./utils/messages');
const { userJoin, getCurrentUser, userLeave, getRoomUsers } = require('./utils/users');

const app = express();
const server = http.createServer(app);
const io = socketio(server);
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public/js')));

const botName = 'GutiBot';
Aliens = [];
Bombs = [];

io.on('connection', socket => {
    socket.on('joinRoom', ({ username, room }) => {
        console.log("User: " + username);
        if (getRoomUsers(room).length >= 2) {
            socket.emit('roomFull', formatMessage(botName, 'Esta sala está llena, abandone la sala e intente otro nombre'));
        } else {
            if (getRoomUsers(room).length == 1) {
                socket.emit('message', formatMessage(botName, '¡Bienvenido!, empezaremos el juego ahora'));
            } else {
                socket.emit('message', formatMessage(botName, '¡Bienvenido! Estamos esperando a otro jugador para empezar la partida'));
            }
            const user = userJoin(socket.id, username, room);

            socket.join(user.room);

            // Hacer broadcast excepto para el cliente que se conecta
            socket.broadcast.to(user.room).emit('message', formatMessage(botName, `${user.username} ha ingresado a la sala`));

            // Enviar información de usuarios y salas
            io.to(user.room).emit('roomUsers', {
                room: user.room,
                users: getRoomUsers(user.room)
            });
        }
    });

    // Empezar juego
    socket.on('start', (message) => {
        const user = getCurrentUser(socket.id);
        if (user != undefined && getRoomUsers(user.room).length > 1) {
            socket.emit('playerOne')
        }
    });

    socket.on('createAliens', (aliens) => {
        const user = getCurrentUser(socket.id);
        Aliens = aliens;
        io.to(user.room).emit('begin', aliens);
    });

    socket.on('update', (data) => {
        const user = getCurrentUser(socket.id);
        Aliens = data.Aliens;
        Bombs = data.Bombs;
        if (user != undefined)
            socket.broadcast.to(user.room).emit('updated', data);
    });

    socket.on('chatMessage', (message) => {
        const user = getCurrentUser(socket.id);
        if (user != undefined)
            io.to(user.room).emit('message', formatMessage(user.username, message));
    });

    socket.on('disconnect', () => {
        const user = userLeave(socket.id);
        if (user != undefined) {
            io.to(user.room).emit('message', formatMessage(botName, `${user.username} ha abandonado la sala`));
            io.to(user.room).emit('dc');
            console.log("Usuario desconectado: " + user.username);
            // Enviar información de usuarios y sala
            io.to(user.room).emit('roomUsers', {
                room: user.room,
                users: getRoomUsers(user.room)
            });
        }
    });
    socket.on('game:over', (decision) => {
        const user = getCurrentUser(socket.id);
        user.decision = decision;
        user.numeroDecisiones++;
        console.log(getRoomUsers(user.room));
        if (user != undefined && getRoomUsers(user.room).length > 1) {
            socket.emit('playerOne');
        }
    });
});


const PORT = process.env.PORT || 3000;
server.listen(PORT, () => console.log(`Servidor corriendo en puerto ${PORT}`))