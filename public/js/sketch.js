var reset_button = document.getElementById("reset_menu")
reset_button.style.visibility = "hidden";

var continue_playing_button = document.getElementById("continue_playing_button")
var leave_room_button = document.getElementById("leave_room_button")

let starts = false;
let delay = 12;
let delayed = 12;
let numAliens = 20;
let playerOne = false;
let Aliens = [];
let otherBombs = [];
let score = 0;
let otherScore = 0;
let grey = [100, 100, 100];
let blue = [51, 168, 199];
let pink = [240, 80, 174];
let yellow = [253, 241, 72];
let explosions = [];
let otherExplosions = [];

// Verificar si el juego puede iniciar
socket.emit("start");

// Solo un jugador crea el alien
socket.on('playerOne', () => {
    playerOne = true;
    socket.emit("createAliens", Aliens);
});

// Al desconectar resetee las variables
socket.on('dc', () => {
    playerOne = false;
    starts = false;
    Aliens = [];
    otherBombs = [];
    Bombs = [];
    score = 0;
    otherScore = 0;
    setup();
});

//Sobreescribe las posiciones x e y de cada alien para hacer match con el servidor y comenzar
socket.on('begin', (aliens) => {
    for (let i = 0; i < numAliens; i++) {
        if (aliens[i] != undefined && Aliens[i] != undefined) {
            Aliens[i].x = aliens[i].x;
            Aliens[i].y = aliens[i].y;
        }
    }
    starts = true;
});

// Actualizar las variables otherShip, otherBombs, y otherAliens (Desde el servidor)
socket.on('updated', (data) => {
    while (data != undefined && data.Aliens.length < Aliens.length) {
        Aliens.splice(Aliens.length - 1, 1);
        for (let i = 0; i < data.Aliens.length; i++) {
            Aliens[i].x = data.Aliens[i].x;
            Aliens[i].y = data.Aliens[i].y;
            Aliens[i].xDir = data.Aliens[i].xDir;
            Aliens[i].yDir = data.Aliens[i].yDir;
        }
    }
    // Si otra nave existe moverla
    if (data.ship != undefined) {
        otherShip.x = data.ship.x;
    } else {
        otherShip = undefined;
    }

    // Hacerle push con todas las bombas del servidor a otherBombs
    if (data.hitBomb > -1) {
        otherBombs.splice(data.hitBomb, 1);
    }
    for (let i = 0; i < data.Bombs.length; i++) {
        if (otherBombs[i] == undefined) {
            let extraBomb = new Bomb(data.Bombs[i].x, data.Bombs[i].y);
            otherBombs.push(extraBomb);
        }
    }

    // Hacer push a otherExplosions
    for (let w = 0; w < data.explosions.length; w++) {
        if (otherExplosions[w] == undefined) {
            let explosion = new Explosion(data.explosions[w].x, data.explosions[w].y);
            otherExplosions.push(explosion);
        }
    }

    otherScore = data.score;
});

function setup() {
    var canvas = createCanvas(700, 500);
    // Mover el canvas para que quede dentro de <div id = "sketch-holder">
    canvas.parent('sketch-holder');
    ship = new Ship(false);
    otherShip = new Ship(true);
    for (let i = 0; i < numAliens; i++) {
        let a = new Alien(random(0, width), random(0, height / 2), 20);
        Aliens.push(a);
    }
    Bombs = [];
    textSize(20);
}

function draw() {
    background('#52e3e1');
    if (!starts) {
        return;
    }
    let string = [
        ["Puntaje: ", grey],
        [score + " ", blue],
        [otherScore, pink],
    ];
    drawtext(10, 20, string);
    // Color alien
    fill(160, 228, 38)

    // Si otra nave existe y no hay aliens jugar otra vez
    if (Aliens.length == 0 && otherShip != undefined && ship != undefined) {
        for (let i = 0; i < numAliens; i++) {
            let a = new Alien(random(0, width), random(0, height / 2), 20);
            Aliens.push(a);
        }
        socket.emit("start");
    }

    //Enviemos una señal al servidor de que el juego ha terminado
    else if (otherShip == undefined || ship == undefined) {
        continue_playing_button.addEventListener('click', function() {
            location.reload();
        })
        leave_room_button.addEventListener('click', function() {
            socket.emit("game:over", false);
            window.location.href = 'index.html';
        })
        reset_button.click();
        socket.emit("game:over", true);
        noLoop();
    }
    //FIN MODIFICACIONES POR MATEO
    else {
        // Mostrar y mover aliens
        for (let i = 0; i < Aliens.length; i++) {
            Aliens[i].show();
            Aliens[i].move();
            if (Aliens[i].hit(ship)) {
                ship = undefined;
                Aliens.splice(i, 1);
                i--;
            }
        }
    }

    // Mostrar y resetear las bombas de otros jugadores
    fill(pink);
    // Mostrar y resetear las explosiones de otros jugadores
    for (let i = 0; i < otherBombs.length; i++) {
        otherBombs[i].show();
        otherBombs[i].move();
        if (otherBombs[i].y < 0) {
            otherBombs.splice(i, 1);
            i--;
            continue;
        }
    }

    // Mostrar y resetear las explosiones de otros jugadores
    for (let i = 0; i < otherExplosions.length; i++) {
        if (otherExplosions[i].show()) {
            otherExplosions.splice(i, 1);
            i--;
        }
    }

    // Mostrar y mover las bombas del jugador
    let hitBomb = -1;
    fill(yellow);
    for (let i = 0; i < Bombs.length; i++) {
        Bombs[i].show();
        Bombs[i].move();

        // Bomba pasa por encima de la pantalla
        if (Bombs[i].y < 0) {
            Bombs.splice(i, 1);
            i--;
            continue;
        }

        // Bomba golpea a alien
        for (let w = 0; w < Aliens.length; w++) {
            if (Bombs[i].hit(Aliens[w])) {
                let explosion = new Explosion(Aliens[w].x, Aliens[w].y)
                explosions.push(explosion);
                Aliens.splice(w, 1);
                Bombs.splice(i, 1);
                hitBomb = i;
                w--;
                i--;
                score++;
                break;
            }
        }
    }
    // Dibujo de la explosión
    for (let w = 0; w < explosions.length; w++) {
        if (explosions[w].show()) {
            explosions.splice(w, 1);
            w--;
        }
    }
    delayed++;


    if (ship != undefined) {
        ship.show();
    }
    if (otherShip != undefined) {
        otherShip.show();
    }

    // emit updates
    let data = {
        Aliens: Aliens,
        Bombs: Bombs,
        ship: ship,
        explosions: explosions,
        score: score,
        hitBomb: hitBomb
    }
    socket.emit("update", data);
}

function mouseMoved() {
    if (ship != undefined) {
        ship.move();
    }
}

// Para móviles
function mouseDragged() {
    if (ship != undefined) {
        ship.move();
    }
}

function touchStarted() {
    if (delayed >= delay && ship != undefined) {
        ball = new Bomb(ship.x + 20, ship.y + 20);
        Bombs.push(ball);
        delayed = 0;
    }
}

function drawtext(x, y, text_array) {
    let pos_x = x;
    for (let i = 0; i < text_array.length; ++i) {
        let part = text_array[i];
        let t = part[0];
        let c = part[1];
        let w = textWidth(t);
        fill(c);
        text(t, pos_x, y);
        pos_x += w;
    }
}