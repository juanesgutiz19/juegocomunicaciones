const chatForm = document.getElementById('chat-form');
const socket = io();
const chatMessages = document.querySelector('.chat-messages');
const roomName = document.getElementById('room-name');
const userList = document.getElementById('users');

const { username, room } = Qs.parse(location.search, {
    ignoreQueryPrefix: true
})

// Unirse a la sala
socket.emit('joinRoom', { username, room });

// Obtener sala y usuarios
socket.on('roomUsers', ({ room, users }) => {
    outputRoomName(room);
    outputUsers(users);
})

// Mensaje de sala llena
socket.on('roomFull', (message) => {
    outputMessage(message);
})

// Mensaje del servidor
socket.on('message', message => {
    outputMessage(message);
    chatMessages.scrollTop = chatMessages.scrollHeight;
})

chatForm.addEventListener('submit', event => {
    event.preventDefault();

    // Obtener el valor del texto
    const msg = event.target.elements.msg.value;

    socket.emit('chatMessage', msg);

    // Limpiar input
    event.target.elements.msg.value = '';
    event.target.elements.msg.focus();
});


// Mensaje de output al DOM
function outputMessage(message) {
    const div = document.createElement('div');
    div.classList.add('message');
    div.innerHTML = `<p class="meta">${message.username} <span>${message.time}</span></p>
    <p class="text">
        ${message.text}
    </p>`;
    document.querySelector('.chat-messages').appendChild(div);
}

//Agregar nombre de sala al DOM
function outputRoomName(room) {
    roomName.innerText = room;
}

function outputUsers(users) {
    userList.innerHTML = `
        ${users.map(user => `<li>${user.username}</li>`).join('')}
    `;
}

window.addEventListener('keydown', function(e) {
    if(e.keyCode == 32 && e.target == document.body) {
      e.preventDefault();
    }
});